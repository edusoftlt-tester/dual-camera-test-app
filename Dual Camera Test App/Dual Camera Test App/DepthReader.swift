//
//  DepthReader.swift
//  Dual Camera Test App
//
//  Created by Eglius on 10/04/2018.
//  Copyright © 2018 Edusoft LT. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class DepthReader {
    
    static func getDepthData(fileUrl: CFURL) -> AVDepthData? {
        
        guard let source = CGImageSourceCreateWithURL(fileUrl, nil) else {
            print("failed load cgImageSource from url")
            return nil
        }
        
        guard let auxDataInfo = CGImageSourceCopyAuxiliaryDataInfoAtIndex(source, 0, kCGImageAuxiliaryDataTypeDisparity) as? [AnyHashable : Any] else {
            print("failed to copy auxiliary data from CGImageSource")
            return nil
        }
        
        var depthData: AVDepthData
        
        do {
            depthData = try AVDepthData(fromDictionaryRepresentation: auxDataInfo)
        } catch {
            print("failed to convert auxiliary data into AVDepthData")
            return nil
        }
        
        if depthData.depthDataType != kCVPixelFormatType_DisparityFloat32 {
            depthData = depthData.converting(toDepthDataType: kCVPixelFormatType_DisparityFloat32)
        }
        
        return depthData
    }
    
    static func getDepthMapImage(depthData: AVDepthData) -> UIImage? {

        let depthDataMap = depthData.depthDataMap
        depthDataMap.normalize()
        let ciImage = CIImage(cvPixelBuffer: depthDataMap)
        let depthMapImage = UIImage(ciImage: ciImage)
        return depthMapImage
    }
    
    static func getDistanceToImagePixel(fileUrl: CFURL, pixelX: Float, pixelY: Float, ImageWidth: Float, ImageHeight: Float) -> Float? {
        guard let depthData = self.getDepthData(fileUrl: fileUrl) else {
            print("failed to get depth data")
            return nil
        }
        
        let depthDataMap = depthData.depthDataMap

        let proportionalX = pixelX / ImageWidth
        let proportionalY = pixelY / ImageHeight
        
        let depthMapWidth = Float(CVPixelBufferGetWidth(depthDataMap))
        let depthMapHeight = Float(CVPixelBufferGetHeight(depthDataMap))
        
        let depthMapX = proportionalX * depthMapWidth
        let depthMapY = proportionalY * depthMapHeight
        
        CVPixelBufferLockBaseAddress(depthDataMap, CVPixelBufferLockFlags(rawValue: 0))
        let floatBuffer = unsafeBitCast(CVPixelBufferGetBaseAddress(depthDataMap), to: UnsafeMutablePointer<Float32>.self)
        let disparity = floatBuffer[Int(depthMapX * depthMapY)]
        let distance = Float(1) / disparity
        return distance
    }
    
}
