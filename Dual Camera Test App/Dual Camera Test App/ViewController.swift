//
//  ViewController.swift
//  Dual Camera Test App
//
//  Created by Eglius on 09/04/2018.
//  Copyright © 2018 Edusoft LT. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    let cameraController = CameraController()
    
    @IBOutlet var capturePreviewView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        func configureCameraController() {
            
            cameraController.prepare {(error) in
                if let error = error {
                    print(error)
                }
            }
        }
        
        configureCameraController()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        try? self.cameraController.displayPreview(on: self.capturePreviewView)
        processImage()
    }
    
    private func processImage() {

        guard let fileUrl = Bundle.main.url(forResource: "IMG_0008", withExtension: "jpg") as CFURL? else {
            print("failed to load file url")
            return
        }

        
        
        let distance = DepthReader.getDistanceToImagePixel(fileUrl: fileUrl, pixelX: 1512.00, pixelY: 2016.00, ImageWidth: 3024.00, ImageHeight: 4032.00)
        print(distance)
    }
    
    
}

